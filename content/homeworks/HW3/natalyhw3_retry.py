# Nataly Moreno (3401098) - Homework 3
#Use image data as the spectrum on which you perform an Inverse Fourier Transform. 
#i.e. Load an image and use the pixel data as FFT bins in an STFT, then using the 
#IFT, produce audio from it. The main complication is segmenting the image pixels 
#into the right size for the IFT. You can interpret the pixels as the magnitude 
#spectrum, or as the real and/or complex part of the complex spectrum.

from pylab import *
from numpy import *
from scipy.io import wavfile
import matplotlib.image as mpimg

#Load the Image
img = mpimg.imread('/Users/NatalyMoreno/Documents/Code/MAT201A/Hw3/Yeyito.jpg')

soundFile = []

for row in range(len(img[:,0])-1):
    mag_spec = img[row,:,1]
    phs_spec = zeros(len(mag_spec))
    X = [np.complex(cos(phs)* mag, sin(phs)* mag) for mag, phs in zip(mag_spec, phs_spec)]
    x = fft.irfft(X)
    soundFile.append(x)

#Flatten the Array
sound_flattened = []
for row in soundFile:
    for col in row:
        sound_flattened.append(col)

sound = asarray(sound_flattened)
wavfile.write('natalymoreno_hw3.wav', 44100, sound)
