Title: Homework 3
Date: 2016-02-16 
Category: Homeworks 
Author: Andres Cabrera 
 
Homework 3 
---------- 
 
* [Mrinalini Anand](http://nbviewer.jupyter.org/gist/Meera94/68a571e278f490c3b5b4) 
* [Jacob Burrows](http://nbviewer.jupyter.org/gist/anonymous/f90a07c3c4972338b6e5) 
* [Qiaodong Cui](http://nbviewer.jupyter.org/gist/cqd123123/1ba1239a3c5da1952ddd) 
* [Hilda He](http://nbviewer.jupyter.org/gist/changer42/24831612174609692c50) 
* [Mark Hirsch](http://nbviewer.jupyter.org/gist/markehirsch/4177c4771dc02147c94d)
* [Woohun Joo](http://nbviewer.jupyter.org/gist/uforange/845e7a84827463500b91)
* [Jingxiang Liu](http://nbviewer.jupyter.org/gist/jingxiang714/167961b304df7fa65ec2) 
* [Lulu Liu](http://nbviewer.jupyter.org/gist/llliiu/42bb5eeb978eed1dcf90) 
* [Weihao Qiu](http://nbviewer.jupyter.org/gist/anonymous/4ec35208436b3dec3732) 
* [Ehsan Sayyad](http://nbviewer.jupyter.org/gist/ehsuun/ebbbe4a61bbdb217638b) 
* [Yitian Shao](http://nbviewer.jupyter.org/gist/maxwellre/1055b1ed13da10595475) 
* [Rachel Sowa](http://nbviewer.jupyter.org/gist/r5sowa/7c5d963b8bb07dcf3fd5) 
* [Ambika Yadav](http://nbviewer.jupyter.org/gist/akibmayadav/a079364b3a238595a20e)
* [Jing Yan](http://nbviewer.jupyter.org/gist/TheUniqueEye/0268f529ff5867166ca8)
* [Junxiang Yao](http://nbviewer.jupyter.org/gist/anonymous/2c5bed8b88b1ace17dcd)
* [Zhenyu Yang](http://nbviewer.jupyter.org/gist/anonymous/5843fc7a507a865e5f85)