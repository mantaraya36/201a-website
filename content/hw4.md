Title: Homework 4
Date: 2016-02-16 
Category: Homeworks 
Author: Andres Cabrera 
 
Homework 4 
---------- 
 
* [Mrinalini Anand](http://nbviewer.jupyter.org/gist/Meera94/d083453f87727ae82d92) 
* [Jacob Burrows](http://nbviewer.jupyter.org/gist/anonymous/42414b7cfc44bde2f2c0) 
* [Qiaodong Cui](http://nbviewer.jupyter.org/gist/cqd123123/985ef0e7a9fcf5286332) 
* [Hilda He](http://nbviewer.jupyter.org/gist/changer42/9f2d2d0d804a4a37a50f) 
* [Mark Hirsch](http://nbviewer.jupyter.org/gist/markehirsch/28fc32af87d15eecf99f)
* [Jingxiang Liu](http://nbviewer.jupyter.org/gist/jingxiang714/3090d26f3d46b473448a) 
* [Lulu Liu](http://nbviewer.jupyter.org/gist/llliiu/9ded28f67983982267d9) 
* [Weihao Qiu](http://nbviewer.jupyter.org/gist/anonymous/44be5b84dbc895b6a49f) 
* [Yitian Shao](http://nbviewer.jupyter.org/gist/maxwellre/43e35c6229dd4fcbbbdc) 
* [Rachel Sowa](http://nbviewer.jupyter.org/gist/r5sowa/c1c9acd2fb8f8847a822) 
* [Ambika Yadav](http://nbviewer.jupyter.org/gist/akibmayadav/fa978a24b905aa3b3ee1)
* [Jing Yan](http://nbviewer.jupyter.org/gist/TheUniqueEye/e454bf05c26632ccd267)
* [Junxiang Yao](http://nbviewer.jupyter.org/gist/wuxiaoci/009c6d871af10bf55fa9)
* [Zhenyu Yang](http://nbviewer.jupyter.org/gist/anonymous/e9998a2568355fd77fe6)