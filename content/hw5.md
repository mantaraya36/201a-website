Title: Homework 5
Date: 2016-03-2 
Category: Homeworks 
Author: Andres Cabrera 
 
Homework 5 
---------- 
 
* [Mrinalini Anand](http://nbviewer.jupyter.org/gist/Meera94/2df70bf3853dc095f24d) 
* [Jacob Burrows](http://nbviewer.jupyter.org/gist/anonymous/9671b10137451be034ef) 
* [Qiaodong Cui](http://nbviewer.jupyter.org/gist/cqd123123/12578fd886acf173e68d) 
* [Hilda He](http://nbviewer.jupyter.org/gist/changer42/bd4febc655ef2a64feca) 
* [Mark Hirsch](http://nbviewer.jupyter.org/gist/markehirsch/3e3b9ebd5e2aebb5b4ac)
* [Woohun Joo](http://nbviewer.jupyter.org/gist/uforange/8f61929a198a81d4e4e7)
* [Jingxiang Liu](http://nbviewer.jupyter.org/gist/jingxiang714/f2234d789b44e9619421) 
* [Lulu Liu](http://nbviewer.jupyter.org/gist/llliiu/4ccde1956502567ea4a6) 
* [Weihao Qiu](http://nbviewer.jupyter.org/gist/anonymous/e527182451f52a76017c) 
* [Ehsan Sayyad](http://nbviewer.jupyter.org/gist/ehsuun/ceef0ad44543fdc65f58) 
* [Yitian Shao](http://nbviewer.jupyter.org/gist/maxwellre/fea3b1d77129eb9e86dc) 
* [Rachel Sowa](http://nbviewer.jupyter.org/gist/r5sowa/00d0043d277d626cf2f9) 
* [Ambika Yadav](http://nbviewer.jupyter.org/gist/akibmayadav/eac67a9590a621ba8b85)
* [Jing Yan](http://nbviewer.jupyter.org/gist/TheUniqueEye/abda18f8052a166bcda5)
* [Junxiang Yao](http://nbviewer.jupyter.org/gist/wuxiaoci/71bfc1f8d675d42ab103)
* [Zhenyu Yang](http://nbviewer.jupyter.org/gist/anonymous/ba308b1638838a227b11)