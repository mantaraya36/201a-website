Title: Info
Date: 2016-01-01
Author: Andres Cabrera
Category: Information

*Lecturer:* Andres Cabrera <andres@mat.ucsb.edu>

*Room:* Elings 2003

*TA:*  Owen Campbell <owencampbell@umail.ucsb.edu>
*TA office hours:* Friday 10am Elings 2003

*Quarter:* Winter 2016

*Mailing list:* [http://lists.create.ucsb.edu/mailman/listinfo/201a](http://lists.create.ucsb.edu/mailman/listinfo/201a)

Summary
===========================================
Processing multi-media data is at the core of today's digital world. From image data mining through computer vision to audio processing and compression for Internet telephony, media signal processing is as ubiquitous as it is inconspicuous. This course hopes to serve as an introduction to the field of processing sound, image and video, from a theoretical perspective, but firmly grounded in practical applications and demonstrations. 
This course will explore fundamental concepts in digital signal processing, multimedia signal processing, and multimedia representations. It will deal with topics like audio and image filtering and feature extraction, gestural input and computer vision.

[Syllabus](|filename|/res/201A-course-outline.pdf)

Homework requirements
---------------------

Please review this document for information about the homework requirements:

[https://docs.google.com/document/d/150Vl0RlaLPR6X7KGkJ2Na49EOKFQ7excgw4108CgyRs/edit?usp=sharing](https://docs.google.com/document/d/150Vl0RlaLPR6X7KGkJ2Na49EOKFQ7excgw4108CgyRs/edit?usp=sharing)




