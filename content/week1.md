Title: Week 1 - Sampling 
Date: 2016-01-04

Notes:

* [Sampling and Quantization]({filename}/notes/Sampling.html)

Ipython notebook:

* [Sampling and Quantization](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Sampling%20and%20Quantization.ipynb?create=1)
* [Image and Audio I/O](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Image%20and%20Audio%20IO.ipynb?create=1)


Reading
-------
*For Tuesday January 12th*

Steven W. Smith, The Scientist and Engineer's Guide to Digital Signal Processing. Chapter 3.
[http://www.dspguide.com/CH3.PDF](http://www.dspguide.com/CH3.PDF)

Further Reading
---------------

[Introduction to Signal Processing, Sophocles J. Orfanidis, Chapter 1](http://eceweb1.rutgers.edu/~orfanidi/intro2sp/)

[http://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem](http://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem)


[http://nbviewer.ipython.org/github/unpingco/Python-for-Signal-Processing/blob/master/Sampling_Theorem.ipynb](http://nbviewer.ipython.org/github/unpingco/Python-for-Signal-Processing/blob/master/Sampling_Theorem.ipynb)

Homework 1
--------
*due: Tuesday Jan 26nd*

Produce a soundfile from image data or vicecersa. Try to condition and select the data to make the results as interesting as possible. Hand in your results as an ipython notebook in nbviewer.


