Title: Week 2 - The Fourier Transform
Date: 2016-01-12


Ipython notebook:

* [Sinusoids, Phasors and the DFT](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Sinusoids%20and%20Phasors.ipynb?create=1)
* [Fourier Analysis](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Fourier Analysis.ipynb)


Homework 2
--------
*due Tuesday February 2rd*

Use image data as the spectrum on which you perform an Inverse Fourier Transform. i.e. Load an image and use the pixel data as FFT bins in an STFT, then using the IFT, produce audio from it. The main complication is segmenting the image pixels into the right size for the IFT. You can interpret the pixels as the magnitude spectrum, or as the real and/or complex part of the complex spectrum.

<!--
Reading
-------

R. Boulanger and V. Lazzarini eds. (2012). [Chapter 7](|filename|/res/APB-Ch7.pdf), Section 7.1. In The Audio Programming Book (pp. 521-528).
-->

