Title: Week 3 - Randomness, Probability and Correlation
Date: 2016-01-19

Notes:

* [Probability](|filename|/notes/Random.html)

Ipython notebook:

* [Random Distributions](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Random%20Distributions.ipynb)
* [Variance and Correlation](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Variance%20and%20Correlation.ipynb)

