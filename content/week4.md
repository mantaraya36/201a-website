Title: Week 4 - Digital Filters
Date: 2016-01-28

Notes:

* [Frequency Domain](|filename|/notes/Frequency_domain.html)
* [Digital Filters](|filename|/notes/Digital_Filters.html)

Ipython notebook:

* [Convolution](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Convolution.ipynb?create=1)
* [Audio Filters](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Audio%20Filters.ipynb?create=1)


Homework 3
--------
*due Tuesday February 9th*

Use cross-correlation (or auto-correlation) to find features or similarities/differences in images. Treat this as an exploratory excersice where you bring in different things to compare, and then analyze/discuss the results. You should also explore analyzing the cross-correlation output matrix to extract the highest/lowest values, in a way that makes sense with your particular material.

Homework 4
--------
*due Thursday February 11th*

Download the three HW4 audio files below. For each clip, compute the DFT of the entire file and then identify the index of the most prominent peak in its magnitude spectrum. The audio files are encoded with a sampling rate of 44100 Hz -- what frequencies (in Hz) correspond to the bins with the most prominent peaks?

Hint: use the *fft.rfft* and *argmax* functions. Compute one real FFT with frame size equal to the number of samples for each file.

Audio Files:

* [glockenspiel.wav](|filename|/res/glockenspiel.wav)
* [piano.wav](|filename|/res/piano.wav)
* [tom.wav](|filename|/res/tom.wav)

Homework 5
--------
*due Thursday February 18th*

Using the same audio files from HW4, compute each clip's autocorrelation function and use it to identify the most salient frequency component (in Hz) in the signal. How do these results compare to your findings in HW4?

Hint: use the *acorr()* function and use the lags associated with the most prominent peaks in the autocorrelation output to calculate the corresponding frequency (recall that these files are sampled at 44100 Hz).


