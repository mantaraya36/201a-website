Title: Week 5 - Image Filters
Date: 2016-02-02

Ipython notebook:

* [Image Filters](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Image%20Filters.ipynb?create=1)


<!--

Homework 6
---------- 
-- *due Tuesday 24th February*

Explore image filters as a creative processing tool. Produce an image that is the creative result of image processing filters. The filters should be applied programmatically (i.e. no Photoshop or Gimp), but try to do things that would be hard to do non-programmatically. Submit as an ipython notebook detailing your goals, and explorations.
-->

Reading
-------
Steven W. Smith, *The Scientist and Engineer's Guide to Digital Signal Processing*. Chapter 6: Convolution. Available from www.dspguide.com.
