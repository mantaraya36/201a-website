Title: Week 6 - Signal modeling
Date: 2016-02-11

Notes:

* [Signal Modeling](|filename|/notes/Modeling.html)

Ipython notebook:

* [Signal modeling](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Signal%20Modeling.ipynb)
* [Signal modeling-Phase Vocoder](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Signal%20Modeling-Phase%20Vocoder.ipynb)


Homework 6
----------
*due: Mon February 22th*

Submit in writing your final project proposal. Provide details on the goals, the deliverables and the technologies to achieve them. 
