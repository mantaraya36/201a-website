Title: Week 9 - Computer Vision
Date: 03-12-2016

Notes:

* [Computer Vision](|filename|/notes/Computer_Vision.html)

Ipython notebook:

* [Computer Vision](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Computer%20vision.ipynb)
* [Computer Vision II](http://nbviewer.ipython.org/github/mantaraya36/201A-ipython/blob/master/Computer%20Vision%20II.ipynb?create=1)

Homework 7
----------

*During class*

Download the notebook and materials:

* [in-class-hw.ipynb](|filename|/res/hw7/in-class-hw.ipynb)
* [melody_1.wav](|filename|/res/hw7/melody_1.wav)
* [melody_2.wav](|filename|/res/hw7/melody_2.wav)
* [melody_1_shifted.wav](|filename|/res/hw7/melody_1_shifted.wav)
* [melody_2_shifted.wav](|filename|/res/hw7/melody_2_shifted.wav)
* [scrambled_1.mat](|filename|/res/hw7/scrambled_1.mat)
* [scrambled_2.mat](|filename|/res/hw7/scrambled_2.mat)
* [scrambled_3.mat](|filename|/res/hw7/scrambled_3.mat)

Then:

 1. Change the name of the notebook to include your name and add your name as a comment on the top of the notebook body.
 2. Complete the missing parts of the notebook. 
 3. At the end of class submit the jupyter notebook file only.
